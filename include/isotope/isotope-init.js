$( function() {
  // init Isotope
  var $container = $('.isotope').isotope({
    itemSelector: '.element-item',
    layoutMode: 'fitRows',
    sortBy : 'random'
  });

  // filter functions
  var filterFns = {
    // show if number is greater than 50
    numberGreaterThan50: function() {
      var number = $(this).find('.number').text();
      return parseInt( number, 10 ) > 50;
    },
  };

  // bind filter button click
  $('#filters').on( 'click', 'button', function() {
    $('.catgryButtons button').removeClass("active");
    $(this).addClass("active");
    var filterValue = $( this ).attr('data-filter');
    // use filterFn if matches value
    filterValue = filterFns[ filterValue ] || filterValue;
    $container.isotope({ filter: filterValue });
  });


  function firstLoadFix() {
    $('.is-checked').click()
  }
  $('.folio').click(function(){
    setTimeout(firstLoadFix, 500)
  })

});